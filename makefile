PROG_NAME	= nordic-cpuid
INSTALL_PATH 	= /usr/bin
install: nordic_cpuid.c
	gcc $< -o $(PROG_NAME)
	mv $(PROG_NAME) $(INSTALL_PATH)

uninstall:
	rm -f $(INSTALL_PATH)/nordic-cpuid

