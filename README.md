this is a small utility tool to retrieve the device-identifier of a nordic device, according to https://infocenter.nordicsemi.com/index.jsp?topic=%2Fcom.nordic.infocenter.nrf52832.ps.v1.1%2Fficr.html

the device identifier will be printed in big-endian order.

# linux:
	install: 
		simply type "sudo make", the program will be compiled and moved to /usr/bin
	uninstall:
		simply type "sudo make uninstall", the program will be removed from /usr/bin
# windows: 
	 no current support for win, but you could compile it manually, or use WSL for windows.
# usage:
	simply tpye nordic-cpuid in your terminal.
