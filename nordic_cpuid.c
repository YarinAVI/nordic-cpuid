#include <stdio.h>
#include <string.h>
#define cmd " --memrd"
#define device_identifier " 0x10000060"
#define device_address " 0x100000A4"
#define bytes " --n 8"


int main() {
    char buffer[2050] = {0};
    FILE * nrfjprog = popen("nrfjprog" cmd device_identifier bytes,"r");
    if(nrfjprog == NULL) {
        fprintf(stderr,"failed to create pipeline\n");
        fclose(nrfjprog);
        return 1;
    }
    fgets(&buffer[0],sizeof(buffer),nrfjprog);
    if(strncmp(&buffer[0],"0x",2) !=0) {
        printf("%s\n",buffer);
        fclose(nrfjprog);
        return 1;
    }
    printf("device identifier(CPUID)-> ");
    int ref = 20;
    for(int i=1;i<=4;i++) {
        printf("%.2s",&buffer[ref-(2*i)]);
    }
    ref = 29;
    for(int i=1;i<=4;i++) {
        printf("%.2s",&buffer[ref-(2*i)]);
    }
    printf("\n");
    fclose(nrfjprog);

    nrfjprog = popen("nrfjprog" cmd device_address bytes,"r");
    if(nrfjprog == NULL) {
        fprintf(stderr,"failed to create pipeline\n");
        fclose(nrfjprog);
        return 1;
    }

        fgets(&buffer[0],sizeof(buffer),nrfjprog);
    if(strncmp(&buffer[0],"0x",2) !=0) {
        printf("%s\n",buffer);
        fclose(nrfjprog);
        return 1;
    }

        printf("device address(MAC address)-> ");
    ref = 20;
    for(int i=1;i<=4;i++) {
        printf("%.2s",&buffer[ref-(2*i)]);
    }
    ref = 29;
    for(int i=1;i<=4;i++) {
        printf("%.2s",&buffer[ref-(2*i)]);
    }
    printf("\n");
    fclose(nrfjprog);    
    return 0;
}